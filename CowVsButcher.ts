/// <reference path="Lib/phaser.d.ts"/>
/// <reference path="States/Preloader.ts"/>
///<reference path="States\GameOver.ts"/>

/* Rename "PhaserTypeScriptTemplate" (this class and the file itself) to the name of the new game */
function RNG(from, to) {
    return Math.random() * (to - from) + from;
}
module CowVsButcher {

    class CowVsButcher extends Phaser.Game {
        game:Phaser.Game;

        constructor(width?:number, height?:number) {
            /* If running on a mobile device, dpr will be the factor by which
             you multiply width and height to get the actual display resolution
             width -> logical width
             width * dpr -> native display width
             */
            var dpr = devicePixelRatio || 1;

            if (!width) {
                width = screen.width * dpr;
            }
            if (!height) {
                height = screen.height * dpr;
            }

            super(width, height, Phaser.CANVAS, 'content', {create: this.create});
        }

        create() {

            this.game.stage.backgroundColor = "#FFF";
            //this.game.scale.maxWidth = 1280;
            //this.game.scale.maxHeight = 720;

            this.game.state.add("Preloader", Preloader, false);
            this.game.state.add("Boot", Boot, false);
            this.game.state.add("Menu", Menu, false);
            this.game.state.add("Game", Game, false);
            this.game.state.add("GameOver", GameOver, false);

            this.game.state.start("Boot");
        }
    }

    window.onload = () => {
        new CowVsButcher(1280,720);
    };
}
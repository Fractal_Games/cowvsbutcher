/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="..\GraphicUtils\BackgroundItem.ts"/>
///<reference path="..\GraphicUtils\ParallaxBackground.ts"/>
///<reference path="..\GraphicUtils\Steak.ts"/>
///<reference path="Level.ts"/>
///<reference path="..\GameObjects\Difficulty.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CowVsButcher;
(function (CowVsButcher) {
    var LevelTwo = (function (_super) {
        __extends(LevelTwo, _super);
        function LevelTwo(game) {
            _super.call(this, game);
        }
        LevelTwo.prototype.create = function () {
            this.levelGroup = this.game.add.group();
            this.background = new CowVsButcher.ParallaxBackground(this.game, "slaughter_background", 2.5, -100, this.levelGroup);
            this.steakHanger = new CowVsButcher.ParallaxBackground(this.game, "platform", 4, -30, this.levelGroup);
            this.steakCollection = [];
            this.randomSteakSpawnTime = this.game.time.now;
            this.ground = new CowVsButcher.ParallaxBackground(this.game, "ground", 8, 586, this.levelGroup);
            _super.prototype.create.call(this);
            //this.levelGroup.addMultiple([this.background.image,this.background.secondImage,this.steakHanger.image,this.steakHanger.secondImage,
            //    this.ground.image,this.ground.secondImage]);
        };
        LevelTwo.prototype.update = function (killerCollection, cow, difficulty) {
            _super.prototype.update.call(this, killerCollection, cow, difficulty);
            this.steakHanger.update();
            this.updateMeatHangers(killerCollection);
            this.spawnPlatform("platformIndoor1", difficulty);
            this.spawnWalls("wallIndoor", difficulty);
            this.updatePlatforms(cow);
            this.updateWalls(cow);
        };
        LevelTwo.prototype.updateMeatHangers = function (killerCollection) {
            if (this.game.time.now > this.randomSteakSpawnTime) {
                this.randomSteakSpawnTime = this.game.time.now + (RNG(3, 6)) * 1000;
                var steak = new CowVsButcher.Steak(this.game, "steak", 4, 50, this.levelGroup);
                steak.sprite.anchor.set(0.5, 0);
                this.steakCollection.push(steak);
                this.levelGroup.addChildAt(steak.sprite, 6);
                this.levelGroup.add(steak.sprite);
            }
            this.steakCollection.forEach(function (steak) {
                steak.update(killerCollection);
                if (steak.sprite.position.x <= -steak.sprite.width) {
                    steak.sprite.alpha = 0;
                    steak.sprite.kill();
                }
            });
            if (this.steakCollection[0] && this.steakCollection[0].sprite.alpha === 0) {
                this.steakCollection.splice(0, 1);
            }
        };
        return LevelTwo;
    })(CowVsButcher.Level);
    CowVsButcher.LevelTwo = LevelTwo;
})(CowVsButcher || (CowVsButcher = {}));
//# sourceMappingURL=LevelTwo.js.map
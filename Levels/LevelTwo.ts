/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="..\GraphicUtils\BackgroundItem.ts"/>
///<reference path="..\GraphicUtils\ParallaxBackground.ts"/>
///<reference path="..\GraphicUtils\Steak.ts"/>
///<reference path="Level.ts"/>
///<reference path="..\GameObjects\Difficulty.ts"/>

module CowVsButcher {
    export class LevelTwo extends Level {

        steakHanger:ParallaxBackground;
        steakCollection:Array<Steak>;
        randomSteakSpawnTime:number;

        constructor(game:Phaser.Game) {
            super(game);
        }

        create() {
            this.levelGroup = this.game.add.group();
            this.background = new ParallaxBackground(this.game, "slaughter_background", 2.5, -100, this.levelGroup);
            this.steakHanger = new ParallaxBackground(this.game, "platform", 4, -30, this.levelGroup);


            this.steakCollection = [];
            this.randomSteakSpawnTime = this.game.time.now;
            this.ground = new ParallaxBackground(this.game, "ground", 8, 586, this.levelGroup);

            super.create();


            //this.levelGroup.addMultiple([this.background.image,this.background.secondImage,this.steakHanger.image,this.steakHanger.secondImage,
            //    this.ground.image,this.ground.secondImage]);

        }

        update(killerCollection:KillerCollection, cow:Cow, difficulty:Difficulty) {
            super.update(killerCollection,cow,difficulty);
            this.steakHanger.update();
            this.updateMeatHangers(killerCollection);
            this.spawnPlatform("platformIndoor1",difficulty);
            this.spawnWalls("wallIndoor",difficulty);
            this.updatePlatforms(cow);
            this.updateWalls(cow);

        }

        private updateMeatHangers(killerCollection:KillerCollection) {
            if (this.game.time.now > this.randomSteakSpawnTime) {
                this.randomSteakSpawnTime = this.game.time.now + (RNG(3,6)) * 1000;
                var steak = new Steak(this.game, "steak", 4, 50, this.levelGroup);
                steak.sprite.anchor.set(0.5, 0);
                this.steakCollection.push(steak);
                this.levelGroup.addChildAt(steak.sprite, 6);
                this.levelGroup.add(steak.sprite);
            }
            this.steakCollection.forEach((steak)=> {
                steak.update(killerCollection);
                if (steak.sprite.position.x <= -steak.sprite.width) {
                    steak.sprite.alpha = 0;
                    steak.sprite.kill();
                    //console.log(this.steakCollection.length);
                }
            });
            if (this.steakCollection[0] && this.steakCollection[0].sprite.alpha === 0) {
                this.steakCollection.splice(0, 1);
            }
        }
    }
}
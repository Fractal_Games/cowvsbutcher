/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="..\GraphicUtils\BackgroundItem.ts"/>
///<reference path="..\GraphicUtils\ParallaxBackground.ts"/>
/// <reference path="../Lib/p2.d.ts"/>
///<reference path="KillerCollection.ts"/>
///<reference path="..\GameObjects\Cow.ts"/>
///<reference path="..\GameObjects\Difficulty.ts"/>
///<reference path="..\GameObjects\Barbeque.ts"/>

module CowVsButcher {
    export class Level {
        game:Phaser.Game;

        background:ParallaxBackground;
        ground:ParallaxBackground;
        levelGroup:Phaser.Group;
        platforms:Array<Phaser.Sprite>;
        walls:Array<Phaser.Sprite>;
        barbeques:Array<Barbeque>;

        randomWallSpawnTime:number;
        randomPlatformSpawnTime:number;
        randomBBQSpawnTime:number;

        constructor(game:Phaser.Game) {
            this.game = game;
        }

        create() {
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.enable(this.ground.image);
            this.game.physics.enable(this.ground.secondImage);
            this.game.physics.arcade.gravity.y = 1500;
            this.ground.image.body.immovable = true;
            this.ground.image.body.moves = false;
            this.ground.secondImage.body.immovable = true;
            this.ground.secondImage.body.moves = false;

            this.randomWallSpawnTime = this.game.time.now;
            this.randomPlatformSpawnTime = this.game.time.now;
            this.randomBBQSpawnTime = this.game.time.now;
            this.platforms = [];
            this.walls = [];
            this.barbeques = [];
        }

        update(killerCollection:KillerCollection, cow:Cow, difficulty:Difficulty) {
            this.ground.update();
            this.background.update();
            this.spawnBBQ(difficulty);
            this.updateBBQ(cow, killerCollection);

        }

        spawnPlatform(key:string, difficulty:Difficulty) {

            if (this.randomPlatformSpawnTime + 1500 < this.game.time.now) {
                this.randomPlatformSpawnTime = this.game.time.now + RNG(difficulty.rngPlatformLow, difficulty.rngPlatformHigh)

                var rngPlatformLevel = this.game.height - Math.floor(RNG(1, 3)) * 180 - 180;
                console.log(rngPlatformLevel);
                var platform = this.game.add.sprite(this.game.width, rngPlatformLevel, key);
                this.levelGroup.addChildAt(platform, 6);
                this.game.physics.arcade.enable(platform);
                platform.scale.set(0.5);
                platform.body.checkCollision.up = true;
                platform.body.checkCollision.down = false;
                platform.body.checkCollision.left = false;
                platform.body.checkCollision.right = false;
                platform.body.immovable = true;
                platform.body.moves = false;

                this.platforms.push(platform);
            }
        }

        updatePlatforms(cow:Cow) {
            this.platforms.forEach((platform)=> {
                platform.position.x -= 8;
                this.game.physics.arcade.collide(cow.states, platform, ()=> {
                    cow.changeToRunning();

                    if (platform.position.x < -platform.width) {
                        this.platforms.splice(this.platforms.indexOf(platform, 1));
                        platform.destroy();
                    }
                });
            }, this);
        }

        spawnWalls(key, difficulty) {
            if (this.randomWallSpawnTime + 2300 < this.game.time.now) {
                this.randomWallSpawnTime = this.game.time.now + RNG(difficulty.rngWallLow, difficulty.rngWallHigh);

                console.log("wall spawned");
                var wall = this.game.add.sprite(this.game.width, 586, key);
                wall.anchor.set(0, 1);
                this.levelGroup.addChildAt(wall, 6);
                this.game.physics.arcade.enable(wall);
                wall.scale.set(RNG(0.7, 0.8));
                wall.body.immovable = true;
                wall.body.moves = false;

                this.walls.push(wall);
                console.log(this.walls.length);
            }
        }

        updateWalls(cow:Cow) {
            this.walls.forEach((wall)=> {
                wall.position.x -= 8;

                this.game.physics.arcade.collide(cow.states, wall, ()=> {
                    cow.changeToRunning();

                    if (wall.position.x < -wall.width) {
                        this.walls.splice(this.walls.indexOf(wall, 1));
                    }
                })
            });
        }

        spawnBBQ(difficulty) {
            if (this.randomBBQSpawnTime + 600 < this.game.time.now) {
                this.randomBBQSpawnTime = this.game.time.now + RNG(difficulty.rngBBQLow, difficulty.rngBBQHigh);

                var bbq = new Barbeque(this.game);
                this.levelGroup.addChildAt(bbq.barbeque, 6);
                this.levelGroup.addChildAt(bbq.smoke, 6);

                this.barbeques.push(bbq);
            }
        }

        updateBBQ(cow:Cow, killerCollection:KillerCollection) {
            this.barbeques.forEach((bbq)=> {
                bbq.update(cow, killerCollection);

                if(bbq.barbeque.position.x<-bbq.barbeque.width){
                    this.barbeques.splice(this.barbeques.indexOf(bbq),1);
                }
            });
        }
    }
}
/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="..\GraphicUtils\BackgroundItem.ts"/>
///<reference path="..\GraphicUtils\ParallaxBackground.ts"/>
///<reference path="..\GraphicUtils\Bird.ts"/>
///<reference path="Level.ts"/>

module CowVsButcher {
    export class LevelOne extends Level {
        game:Phaser.Game;

        background:ParallaxBackground;
        mountains_front:ParallaxBackground;
        mountains_back:ParallaxBackground;
        grass:ParallaxBackground;

        rockCollection:Array<BackgroundItem>;
        randomRockSpawnTime:number;

        treeCollection:Array<BackgroundItem>;
        randomTreeSpawnTime:number;

        birdCollection:Array<Bird>;
        randomBirdSpawnTime:number;

        constructor(game:Phaser.Game) {
            super(game);
        }

        create() {

            this.levelGroup = this.game.add.group();

            this.background = new ParallaxBackground(this.game, "sky", 2.5, -100, this.levelGroup);

            this.mountains_back = new ParallaxBackground(this.game, "mountains_back", 0.5, 200, this.levelGroup);
            this.mountains_front = new ParallaxBackground(this.game, "mountains_front", 1, 300, this.levelGroup);
            this.treeCollection = [];
            this.rockCollection = [];
            this.randomTreeSpawnTime = this.game.time.now;
            this.randomRockSpawnTime = this.game.time.now;

            this.ground = new ParallaxBackground(this.game, "floor_outdoor", 8, 586, this.levelGroup);
            this.grass = new ParallaxBackground(this.game, "grass", 8, 570, this.levelGroup);

            this.birdCollection = [];
            this.randomBirdSpawnTime = this.game.time.now;

            super.create();
        }

        update(killerCollection:KillerCollection, cow:Cow, difficulty:Difficulty) {

            super.update(killerCollection, cow, difficulty);
            this.updateRocks();
            this.updateTrees();
            this.mountains_front.update();
            this.mountains_back.update();
            this.grass.update();
            this.updateBirds(killerCollection);

            this.spawnPlatform("platformOutdoor",difficulty);
            this.spawnWalls("wallOutdoor",difficulty)
            this.updatePlatforms(cow);
            this.updateWalls(cow);
        }

        private updateTrees() {
            if (this.game.time.now > this.randomTreeSpawnTime) {
                this.randomTreeSpawnTime = this.game.time.now + (RNG(1, 5)) * 1000;
                var tree = new BackgroundItem(this.game, "tree", 4, 595, this.levelGroup);
                this.treeCollection.push(tree);
                this.levelGroup.addChildAt(tree.image, 6);
            }
            this.treeCollection.forEach((tree)=> {
                if (tree.image.position.x <= -tree.image.width) {
                    tree.image.alpha = 0;
                    tree.image.kill();
                }
                tree.update();
            });
            if (this.treeCollection[0] && this.treeCollection[0].image.alpha === 0) {
                this.treeCollection.splice(0, 1);
            }
        }

        private updateRocks() {
            if (this.game.time.now > this.randomRockSpawnTime) {
                this.randomRockSpawnTime = this.game.time.now + (RNG(1, 3)) * 1000;
                var randomRockNumber = (RNG(1, 3)) | 0;
                var rock = new BackgroundItem(this.game, "rock" + randomRockNumber.toString(), 8, 710, this.levelGroup);
                this.rockCollection.push(rock);
            }
            this.rockCollection.forEach((rock)=> {
                rock.update();
                if (rock.image.position.x <= -rock.image.width) {
                    rock.image.alpha = 0;
                    rock.image.kill();
                }
            });
            if (this.rockCollection[0] && this.rockCollection[0].image.alpha === 0) {
                this.rockCollection.splice(0, 1);
            }
        }

        private updateBirds(killerCollection:KillerCollection) {
            if (this.game.time.now > this.randomBirdSpawnTime) {
                this.randomBirdSpawnTime = this.game.time.now + (RNG(5, 8)) * 1000;
                var bird = new Bird(this.game, "Bird", 12, 120, this.levelGroup);
                this.birdCollection.push(bird);
                this.levelGroup.addChildAt(bird.sprite, 6);
                this.levelGroup.add(bird.sprite);
            }
            this.birdCollection.forEach((bird)=> {
                bird.update(killerCollection);
                if (bird.sprite.position.x <= 0) {
                    bird.sprite.alpha = 0;
                    bird.sprite.kill();
                }
            });
            if (this.birdCollection[0] && this.birdCollection[0].sprite.alpha === 0) {
                this.birdCollection.splice(0, 1);
            }

        }

    }

}
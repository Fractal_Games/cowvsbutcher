/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="..\GraphicUtils\BackgroundItem.ts"/>
///<reference path="..\GraphicUtils\ParallaxBackground.ts"/>
///<reference path="..\GraphicUtils\Bird.ts"/>
///<reference path="Level.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CowVsButcher;
(function (CowVsButcher) {
    var LevelOne = (function (_super) {
        __extends(LevelOne, _super);
        function LevelOne(game) {
            _super.call(this, game);
        }
        LevelOne.prototype.create = function () {
            this.levelGroup = this.game.add.group();
            this.background = new CowVsButcher.ParallaxBackground(this.game, "sky", 2.5, -100, this.levelGroup);
            this.mountains_back = new CowVsButcher.ParallaxBackground(this.game, "mountains_back", 0.5, 200, this.levelGroup);
            this.mountains_front = new CowVsButcher.ParallaxBackground(this.game, "mountains_front", 1, 300, this.levelGroup);
            this.treeCollection = [];
            this.rockCollection = [];
            this.randomTreeSpawnTime = this.game.time.now;
            this.randomRockSpawnTime = this.game.time.now;
            this.ground = new CowVsButcher.ParallaxBackground(this.game, "floor_outdoor", 8, 586, this.levelGroup);
            this.grass = new CowVsButcher.ParallaxBackground(this.game, "grass", 8, 570, this.levelGroup);
            this.birdCollection = [];
            this.randomBirdSpawnTime = this.game.time.now;
            _super.prototype.create.call(this);
        };
        LevelOne.prototype.update = function (killerCollection, cow, difficulty) {
            _super.prototype.update.call(this, killerCollection, cow, difficulty);
            this.updateRocks();
            this.updateTrees();
            this.mountains_front.update();
            this.mountains_back.update();
            this.grass.update();
            this.updateBirds(killerCollection);
            this.spawnPlatform("platformOutdoor", difficulty);
            this.spawnWalls("wallOutdoor", difficulty);
            this.updatePlatforms(cow);
            this.updateWalls(cow);
        };
        LevelOne.prototype.updateTrees = function () {
            if (this.game.time.now > this.randomTreeSpawnTime) {
                this.randomTreeSpawnTime = this.game.time.now + (RNG(1, 5)) * 1000;
                var tree = new CowVsButcher.BackgroundItem(this.game, "tree", 4, 595, this.levelGroup);
                this.treeCollection.push(tree);
                this.levelGroup.addChildAt(tree.image, 6);
            }
            this.treeCollection.forEach(function (tree) {
                if (tree.image.position.x <= -tree.image.width) {
                    tree.image.alpha = 0;
                    tree.image.kill();
                }
                tree.update();
            });
            if (this.treeCollection[0] && this.treeCollection[0].image.alpha === 0) {
                this.treeCollection.splice(0, 1);
            }
        };
        LevelOne.prototype.updateRocks = function () {
            if (this.game.time.now > this.randomRockSpawnTime) {
                this.randomRockSpawnTime = this.game.time.now + (RNG(1, 3)) * 1000;
                var randomRockNumber = (RNG(1, 3)) | 0;
                var rock = new CowVsButcher.BackgroundItem(this.game, "rock" + randomRockNumber.toString(), 8, 710, this.levelGroup);
                this.rockCollection.push(rock);
            }
            this.rockCollection.forEach(function (rock) {
                rock.update();
                if (rock.image.position.x <= -rock.image.width) {
                    rock.image.alpha = 0;
                    rock.image.kill();
                }
            });
            if (this.rockCollection[0] && this.rockCollection[0].image.alpha === 0) {
                this.rockCollection.splice(0, 1);
            }
        };
        LevelOne.prototype.updateBirds = function (killerCollection) {
            if (this.game.time.now > this.randomBirdSpawnTime) {
                this.randomBirdSpawnTime = this.game.time.now + (RNG(5, 8)) * 1000;
                var bird = new CowVsButcher.Bird(this.game, "Bird", 12, 120, this.levelGroup);
                this.birdCollection.push(bird);
                this.levelGroup.addChildAt(bird.sprite, 6);
                this.levelGroup.add(bird.sprite);
            }
            this.birdCollection.forEach(function (bird) {
                bird.update(killerCollection);
                if (bird.sprite.position.x <= 0) {
                    bird.sprite.alpha = 0;
                    bird.sprite.kill();
                }
            });
            if (this.birdCollection[0] && this.birdCollection[0].sprite.alpha === 0) {
                this.birdCollection.splice(0, 1);
            }
        };
        return LevelOne;
    })(CowVsButcher.Level);
    CowVsButcher.LevelOne = LevelOne;
})(CowVsButcher || (CowVsButcher = {}));
//# sourceMappingURL=LevelOne.js.map
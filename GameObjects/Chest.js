/// <reference path="../Lib/p2.d.ts"/>
/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="Cow.ts"/>
///<reference path="..\GraphicUtils\ItemButton.ts"/>
var CowVsButcher;
(function (CowVsButcher) {
    var Chest = (function () {
        function Chest(game) {
            this.game = game;
            var x = RNG(900, 1280);
            this.chests = this.game.add.sprite(x, 0, "Chests");
            this.chests.anchor.set(0.5, 0.5);
            this.game.physics.enable(this.chests);
            this.chests.body.setSize(40, 40, 0, 60);
            this.chests.body.gravity.y = -1000;
            this.chests.body.mass = 0;
            this.chests.animations.add("ammo", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 30);
            this.chests.animations.add("GMO", [10, 11, 12, 13, 14, 15, 16, 17, 18, 19], 30);
            this.chests.animations.add("grenade", [20, 21, 22, 23, 24, 25, 26, 27, 28, 29], 30);
            var randomAnimation = Math.random();
            if (randomAnimation < 0.49) {
                this.chests.animations.play("grenade", null, true);
            }
            else if (randomAnimation >= 0.49 && randomAnimation <= 0.9) {
                this.chests.animations.play("ammo", null, true);
            }
            else {
                this.chests.animations.play("GMO", null, true);
            }
            console.log("chest created");
        }
        Chest.prototype.update = function (cow, grenadeButton, shotgunButton, gmoButton) {
            var _this = this;
            this.game.physics.arcade.overlap(this.chests, cow.states, function () {
                if (_this.chests.animations.currentAnim.name === "ammo") {
                    shotgunButton.pickUpItem(8);
                    _this.chests.body.setSize(0, 0);
                    _this.chests.alpha = 0;
                    _this.chests.destroy();
                }
                else if (_this.chests.animations.currentAnim.name === "grenade") {
                    grenadeButton.pickUpItem(3);
                    _this.chests.body.setSize(0, 0);
                    _this.chests.destroy();
                    _this.chests.alpha = 0;
                }
                else if (_this.chests.animations.currentAnim.name === "GMO") {
                    gmoButton.pickUpItem(1);
                    _this.chests.body.setSize(0, 0);
                    _this.chests.destroy();
                    _this.chests.alpha = 0;
                }
            });
            if (this.chests.body && this.chests !== null) {
                this.chests.body.velocity.x = -800;
            }
            //this.chests.body.velocity.y=+500;
        };
        return Chest;
    })();
    CowVsButcher.Chest = Chest;
})(CowVsButcher || (CowVsButcher = {}));
//# sourceMappingURL=Chest.js.map
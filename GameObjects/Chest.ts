/// <reference path="../Lib/p2.d.ts"/>
/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="Cow.ts"/>
///<reference path="..\GraphicUtils\ItemButton.ts"/>


module CowVsButcher {
    export class Chest {

        game:Phaser.Game;
        chests:Phaser.Sprite;

        constructor(game:Phaser.Game) {
            this.game = game;

            var x = RNG(900,1280);
            this.chests = this.game.add.sprite(x, 0, "Chests");
            this.chests.anchor.set(0.5, 0.5);
            this.game.physics.enable(this.chests);
            this.chests.body.setSize(40, 40, 0, 60);
            this.chests.body.gravity.y = -1000;
            this.chests.body.mass=0;
            this.chests.animations.add("ammo", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 30);
            this.chests.animations.add("GMO", [10, 11, 12, 13, 14, 15, 16, 17, 18, 19], 30);
            this.chests.animations.add("grenade", [20, 21, 22, 23, 24, 25, 26, 27, 28, 29], 30);

            var randomAnimation = Math.random();
            if (randomAnimation < 0.49) {
                this.chests.animations.play("grenade", null, true);
            }
            else if (randomAnimation >= 0.49 && randomAnimation <= 0.9) {
                this.chests.animations.play("ammo", null, true);
            }
            else {
                this.chests.animations.play("GMO", null, true);
            }

            console.log("chest created");

        }

        update(cow:Cow, grenadeButton:ItemButton, shotgunButton:ItemButton, gmoButton:ItemButton) {
            this.game.physics.arcade.overlap(this.chests,cow.states,()=>{
             if(this.chests.animations.currentAnim.name ==="ammo"){
                 shotgunButton.pickUpItem(8);
                 this.chests.body.setSize(0,0);
                 this.chests.alpha=0;
                 this.chests.destroy();
             }

             else if(this.chests.animations.currentAnim.name ==="grenade"){
                 grenadeButton.pickUpItem(3);

                 this.chests.body.setSize(0,0);
                 this.chests.destroy();
                 this.chests.alpha=0;
             }
                else if(this.chests.animations.currentAnim.name ==="GMO"){
                    gmoButton.pickUpItem(1);

                    this.chests.body.setSize(0,0);
                    this.chests.destroy();
                    this.chests.alpha=0;
                }
            });
            if(this.chests.body && this.chests!==null) {
                this.chests.body.velocity.x = -800;
            }
            //this.chests.body.velocity.y=+500;

        }
    }
}
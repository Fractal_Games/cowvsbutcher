/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="Cow.ts"/>
module CowVsButcher{
    export class Barbeque {
        game:Phaser.Game;
        smoke:Phaser.Sprite;
        barbeque:Phaser.Sprite;

        constructor(game:Phaser.Game){
            this.game = game;

            this.barbeque = this.game.add.sprite(this.game.width*1.3,600,"barbeque");
            this.barbeque.anchor.set(0.5,0.5);
            this.barbeque.scale.set(0.5);
            this.game.physics.arcade.enable(this.barbeque);
            this.barbeque.body.moves = false;
            this.barbeque.body.immovable = true;

            this.smoke = this.game.add.sprite(this.game.width*1.3,500, "smoke");
            this.smoke.animations.add("smoking");
            this.smoke.anchor.set(0.5,0.5);
            this.smoke.animations.play("smoking",10, true);
            this.smoke.alpha = 0.7;
            this.smoke.rotation = 90;
        }

        update(cow:Cow, killerCollection:KillerCollection){
            this.smoke.position.x-=8;
            this.barbeque.position.x-=8;

            this.game.physics.arcade.collide(cow.states,this.barbeque,()=>{
                console.log("collide");
                cow.states.animations.play("burning", null,true);
                cow.states.body.immovable = true;
                cow.states.body.moves = false;
                killerCollection.gameOver = true;
            });
        }

    }
}
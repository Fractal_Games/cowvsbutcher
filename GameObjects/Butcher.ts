/// <reference path="../Lib/phaser.d.ts"/>
/// <reference path="../Lib/p2.d.ts"/>
///<reference path="..\Levels\Level.ts"/>
///<reference path="Cow.ts"/>
module CowVsButcher {
    export enum ButcherStates {IDLE, RUNNING, PICKING_ITEM, SHOTGUN_DEATH, GRENADE_DEATH, KNIFE_DEATH, DEAD, THROWN_BY_GMO}


    export class Butcher {
        game:Phaser.Game;
        states:Phaser.Sprite;
        velocity:number;
        isDead:boolean;
        currentState:ButcherStates;
        states2:Phaser.Sprite;
        bloodThrown:boolean;

        constructor(game:Phaser.Game, velocity) {
            this.game = game;
            this.velocity = velocity;
            this.isDead = false;
            this.bloodThrown = false;
            this.currentState = ButcherStates.RUNNING;

            this.states = this.game.add.sprite(1300, 400, "Butcher");
            this.states.animations.add("attack", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 30);
            this.states.animations.add("knifeDeath", [11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22], 30);
            this.states.animations.add("grenadeDeath", [23, 24, 25, 26, 27, 28, 29, 30, 31], 30);
            this.states.animations.add("idle", [32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45], 30);
            this.states.animations.add("run", [46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56], 30);
            this.states.animations.add("shotgunDeath", [57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68], 30);

            this.states2 = this.game.add.sprite(1300, 400, "Butcher2");
            this.states2.animations.add("gmo_death", [0, 1, 2, 3, 4, 5, 6, 7, 8], 30);
            this.states2.animations.add("sawDeath", [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23], 30);

            this.states2.scale.set(-1, 1);
            this.states2.anchor.set(0.5, 0.5);
            this.states2.alpha = 0;

            if (Math.random() < 0.5) {
                this.states.animations.play("run", null, true);
            }

            else {
                this.states.animations.play("attack", null, true);
            }
            this.game.physics.arcade.enable(this.states);
            this.states.scale.set(-1, 1);
            this.states.anchor.set(0.5, 0.5);
            this.states.body.setSize(100, 100, -100, -50);
            this.states.body.fixedRotation = true;
            this.states.body.gravity.y = 1000;
        }

        update(currentLevel:Level, killerCollection:KillerCollection, butcherList:Array<Butcher>, cow:Cow,
               thrownItems:Array<Phaser.Sprite>, pickUpItems:Array<Phaser.Sprite>) {
            this.states.body.velocity.x = -this.velocity;

            this.game.physics.arcade.collide(this.states, currentLevel.ground.secondImage);
            this.game.physics.arcade.collide(this.states, currentLevel.ground.image);

            this.butcherCowCollision(cow, killerCollection);
            this.butcherItemCollision(thrownItems, killerCollection, pickUpItems);
            this.shotgunAndSawDeath(killerCollection);
            this.gmoBlood(killerCollection);
            this.removeButcherFromGame(killerCollection, butcherList);
        }

        private removeButcherFromGame(killerCollection, butcherList) {
            if (this.states.body.x < 0 || this.states2.position.x < 0) {
                this.currentState = ButcherStates.DEAD;
            }

            if (this.currentState === ButcherStates.DEAD || this.currentState === ButcherStates.GRENADE_DEATH) {
                killerCollection.killerGroup.remove(this);
                butcherList.splice(butcherList.indexOf(this), 1);
                this.states.destroy();
            }
        }

        changeToAttack() {
            this.states.animations.play("attack", null, false)
                .onComplete.addOnce(()=> {
                    this.states.animations.play("run", null, true);
                }, this);
        }


        private butcherCowCollision(cow, killerCollection) {
            this.game.physics.arcade.overlap(this.states, cow.states, ()=> {
                if (cow.currentState !== CowStates.GMO) {
                    this.changeToAttack();
                    cow.die();
                    killerCollection.gameOver=true;
                }
                else {
                    cow.states.body.immovable = true;
                    cow.states.body.moves = false;

                    if (this.currentState !== ButcherStates.THROWN_BY_GMO) {
                        this.thrownByGMO();
                    }
                }
            }, null, this);
        }

        deathByItem(key) {
            this.states.animations.play(key + "Death", null, false);
            this.isDead = true;
            this.states.body.setSize(0, 0, 0, 0);

            if (key === "grenade") {
                this.states.animations.currentAnim.onComplete.add(()=> {
                    this.states.alpha = 0;
                    this.currentState = ButcherStates.GRENADE_DEATH;
                }, this);
            }

            else if (key === "knife") {
                this.currentState = ButcherStates.KNIFE_DEATH;
            }

            else if (key === "shotgun") {
                this.currentState = ButcherStates.SHOTGUN_DEATH;
                this.states.animations.currentAnim.onComplete.add(()=> {
                    this.states.bringToTop();
                }, this);
            }
        }

        private butcherItemCollision(thrownItems, killerCollection, pickUpItems) {
            thrownItems.forEach((item)=>{
                this.game.physics.arcade.overlap(item, this.states, ()=> {
                    var shouldThrowKnifeRNG = Math.random();
                    this.deathByItem(item.key);
                    thrownItems.splice(thrownItems.indexOf(item), 1);
                    item.destroy();

                    var position = this.states.body;
                    var offset = 100;

                    if (item.key === "grenade") {
                        killerCollection.boneEmitter.start(position.x + offset, position.y - offset + 10, true, 2000, Math.abs(RNG(1, 3)));
                    }

                    killerCollection.bloodEmitter.start(position.x + offset, position.y - offset + 10, true, 800, 50);

                    if (shouldThrowKnifeRNG < 0.0) {
                        this.spawnPickupKnife(position.x + offset, position.y - offset, pickUpItems);
                    }

                    killerCollection.updateKillerText();
                }, null, this);


            }, this);
        }

        spawnPickupKnife(x:number, y:number, pickupItems) {
            var knife = this.game.add.sprite(x, y, "knife");

            this.game.physics.arcade.enable(knife);

            knife.anchor.set(0.5, 0.5);
            //knife.body.velocity.x = -100;
            knife.body.velocity.y = -500;
            knife.body.mass = 0;
            knife.body.collideWorldBounds = false;
            knife.body.setSize(20, 30, 0, 0);

            pickupItems.push(knife);
        }

        private shotgunAndSawDeath(killerCollection:KillerCollection) {
            if (this.states.body.x < 100 && this.currentState === ButcherStates.SHOTGUN_DEATH) {

                this.states.alpha = 0;
                this.states.body.setSize(0, 0);
                this.states2.alpha = 1;
                this.states2.position.x = this.states.position.x;
                this.states2.position.y = this.states.position.y;
                var positionTween = this.game.add.tween(this.states2.position);
                positionTween.to({x: -500}, 2000, Phaser.Easing.Default, true);
                killerCollection.killerGroup.bringToTop(this.states2);

                this.states2.animations.play("sawDeath", null, false);
                this.currentState = ButcherStates.DEAD;
                killerCollection.updateKillerText();
            }

            if (this.states2.alpha === 1) {
                this.states2.rotation -= 0.05;
            }

            if (this.states2.alpha === 1 && this.states2.position.x < 200 && !this.bloodThrown && this.currentState!==ButcherStates.THROWN_BY_GMO) {
                this.bloodThrown = true;
                killerCollection.killerGroup.bringToTop(killerCollection.bloodEmitter.emitter);
                killerCollection.bloodEmitter.start(70, 480, true, 800, 500);
                killerCollection.killerGroup.bringToTop(this.states2);
                killerCollection.thirdSaw.triggerSaw();
            }
        }

        private gmoBlood(killerCollection:KillerCollection){
            if (this.states2.alpha === 1 && this.states2.position.x < 200 && !this.bloodThrown && this.currentState===ButcherStates.THROWN_BY_GMO) {
                this.bloodThrown = true;
                killerCollection.killerGroup.bringToTop(killerCollection.bloodEmitter.emitter);
                killerCollection.bloodEmitter.start(70, 80, true, 800, 500);
                killerCollection.killerGroup.sendToBack(this.states2);
                killerCollection.firstSaw.changeToBloodierImage();
                killerCollection.secondSaw.triggerSaw();
                killerCollection.updateKillerText();
            }
        }

        thrownByGMO() {
            this.game.physics.arcade.enable(this.states2);
            this.states.alpha = 0;
            this.states.body.setSize(0, 0);
            this.states2.alpha = 1;
            this.states2.position.x = this.states.position.x;
            this.states2.position.y = this.states.position.y;
            this.states2.animations.play("gmo_death", null, true);
            this.states2.body.velocity.y = -1200;
            this.states2.body.velocity.x = -500;

            this.currentState = ButcherStates.THROWN_BY_GMO;
        }
    }
}

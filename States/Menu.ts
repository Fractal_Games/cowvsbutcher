/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="..\GraphicUtils\Saw.ts"/>
///<reference path="..\GameObjects\Cow.ts"/>
///<reference path="..\GameObjects\Butcher.ts"/>
///<reference path="..\GraphicUtils\BloodEmitter.ts"/>
module CowVsButcher {
    export class Menu extends Phaser.State {
        saw1:Saw;
        saw2:Saw;
        saw3:Saw;

        sawGroup:Phaser.Group;

        create() {
            this.game.add.image(0, 0, "MainScreen");

            var playButton = this.game.add.button(this.game.width / 2, this.game.height * 0.55, "PlayButton", this.startGame, this);
            playButton.anchor.set(0.5);
            playButton.input.priorityID=1;
            var quitButton = this.game.add.button(this.game.width / 2, this.game.height * 0.8, "QuitButton", this.exitGame, this);
            quitButton.anchor.set(0.5);
            quitButton.input.priorityID=1;

            var bloodEmitter = new BloodEmitter(this.game, 50, 1000);
            bloodEmitter.emitter.x = this.game.width * 0.18;
            bloodEmitter.emitter.y = this.game.height * 0.18;
            bloodEmitter.emitter.start(false, 2000, 0, 5000, false);

            var bloodEmitter2 = new BloodEmitter(this.game, 50, 1000);
            bloodEmitter2.emitter.x = this.game.width * 0.88;
            bloodEmitter2.emitter.y = this.game.height * 0.4;
            bloodEmitter2.emitter.start(false, 2000, 0, 5000, false);


            this.saw1 = new Saw(this.game, "bloody_saw", "bloody_saw", "bloodier_saw", 230, 100, 0.3, this.sawGroup);
            this.saw2 = new Saw(this.game, "bloodier_saw", "bloody_saw", "bloodier_saw", 1130, 230, 0.4, this.sawGroup);
            this.saw3 = new Saw(this.game, "bloodier_saw", "bloody_saw", "bloodier_saw", 50, 670, 0.9, this.sawGroup);


            var cow = new Cow(this.game);
            cow.states.position.set(250, 500);
            cow.states.scale.set(1.4);
            cow.states.body.immovable = true;
            cow.states.body.moves = false;
            cow.changeAnimationRepeating("idle");

            var butcher = new Butcher(this.game,0);
            butcher.states.position.set(1100, 600);
            butcher.states.scale.set(-1.4, 1.4);
            butcher.states.animations.play("idle",null,true);
            butcher.states.body.immovable=true;
            butcher.states.body.moves=false;
        }

        update() {
            this.saw1.defaultImage.rotation += 0.04;
            this.saw2.defaultImage.rotation -= 0.05;
            this.saw3.defaultImage.rotation += 0.02;
        }

        startGame() {
            this.game.state.start("Game");
        }

        exitGame() {
            this.game.destroy();
        }

    }
}
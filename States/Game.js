/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="../GraphicUtils/Blood.ts"/>
///<reference path="../GraphicUtils/BloodEmitter.ts"/>
///<reference path="../GraphicUtils/Emitter.ts"/>
///<reference path="../GraphicUtils/ParallaxBackground.ts"/>
///<reference path="..\GraphicUtils\BackgroundItem.ts"/>
///<reference path="..\GraphicUtils\Saw.ts"/>
///<reference path="..\Levels\LevelOne.ts"/>
///<reference path="..\Levels\LevelTwo.ts"/>
///<reference path="..\Levels\KillerCollection.ts"/>
///<reference path="..\GameObjects\Cow.ts"/>
///<reference path="..\GraphicUtils\ItemButton.ts"/>
///<reference path="..\GameObjects\Butcher.ts"/>
///<reference path="..\GameObjects\Chest.ts"/>
///<reference path="..\GameObjects\Difficulty.ts"/>
///<reference path="..\GameObjects\Barbeque.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CowVsButcher;
(function (CowVsButcher) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this);
        }
        Game.prototype.create = function () {
            this.difficulties = [];
            this.difficulties.push(new CowVsButcher.Difficulty(5, 10, 20, 3, 10, 300, 1500, 3000, 2500, 5000, 2000, 3000));
            this.difficulties.push(new CowVsButcher.Difficulty(1, 5, 15, 2, 8, 400, 1000, 2200, 2000, 4000, 1500, 2500));
            this.difficulties.push(new CowVsButcher.Difficulty(2, 4, 10, 0, 5, 500, 800, 1800, 1800, 3000, 1000, 1500));
            this.currentDifficulty = this.difficulties[0];
            this.shouldChangeDifficulty = true;
            this.levelOne = new CowVsButcher.LevelOne(this.game);
            this.levelTwo = new CowVsButcher.LevelTwo(this.game);
            this.killerCollection = new CowVsButcher.KillerCollection(this.game);
            this.thrownItems = [];
            this.pickupItems = [];
            this.chestList = [];
            this.topGroup = this.game.add.group();
            this.game.time.advancedTiming = true;
            this.game.stage.backgroundColor = "#FFF";
            this.levelOne.create();
            this.levelTwo.create();
            this.killerCollection.create();
            this.currentLevel = this.levelTwo;
            this.game.world.bringToTop(this.levelTwo.levelGroup);
            this.game.world.bringToTop(this.killerCollection.killerGroup);
            this.cow = new CowVsButcher.Cow(this.game);
            this.topGroup.addMultiple([this.cow.states, this.cow.states2]);
            this.butcherList = [];
            //UI
            this.grenadeButton = new CowVsButcher.ItemButton(this.game, 1200, 650, "grenadeButton", 3, this.cow, "throwGrenade");
            this.shotgunButton = new CowVsButcher.ItemButton(this.game, 1100, 650, "shotgunButton", 8, this.cow, "shotgun");
            this.knifeButton = new CowVsButcher.ItemButton(this.game, 1000, 650, "knifeButton", 0, this.cow, "throwKnife");
            this.gmoButton = new CowVsButcher.ItemButton(this.game, 800, 650, "gmoButton", 1, this.cow, "gmo");
            this.topGroup.addMultiple([this.grenadeButton, this.grenadeButton.spriteBW, this.grenadeButton.ammoText, this.knifeButton, this.knifeButton.spriteBW, this.knifeButton.ammoText, this.shotgunButton, this.shotgunButton.spriteBW, this.shotgunButton.ammoText, this.gmoButton, this.gmoButton.spriteBW, this.gmoButton.ammoText]);
            this.randomChestsSpawnTime = this.game.time.now + Phaser.Timer.SECOND * 10;
            this.randomButcherSpawnTime = this.game.time.now + Phaser.Timer.SECOND * 10;
            //this.game.time.events.add(2000,()=>{
            //    this.testPlatform = this.game.add.sprite(120,300,"platformIndoor1");
            //    this.currentLevel.levelGroup.addChildAt(this.testPlatform,6);
            //    this.game.physics.arcade.enable(this.testPlatform);
            //    this.testPlatform.scale.set(0.5,0.5);
            //    this.testPlatform.body.checkCollision.up = true;
            //    this.testPlatform.body.checkCollision.down = false;
            //    this.testPlatform.body.checkCollision.left = false;
            //    this.testPlatform.body.checkCollision.right = false;
            //    this.testPlatform.body.immovable = true;
            //    this.testPlatform.body.moves = false;
            //},this);
            this.game.world.bringToTop(this.topGroup);
        };
        Game.prototype.update = function () {
            var _this = this;
            if (!this.killerCollection.gameOver) {
                this.chestList.forEach(function (chest) {
                    chest.update(_this.cow, _this.grenadeButton, _this.shotgunButton, _this.gmoButton);
                });
                this.cowThrowItems();
                this.cow.update(this.currentLevel);
                this.killerCollection.update(this.currentLevel);
                this.currentLevel.update(this.killerCollection, this.cow, this.currentDifficulty);
                this.butcherList.forEach(function (butch) {
                    butch.update(_this.currentLevel, _this.killerCollection, _this.butcherList, _this.cow, _this.thrownItems, _this.pickupItems);
                }, this);
                this.spawnChests();
                this.spawnButchers();
                this.pickupItemsUpdate();
                this.thrownItemsUpdate();
                this.updateDifficulties();
            }
            else {
                this.game.physics.arcade.collide(this.cow.cowHead, this.currentLevel.ground.image, function () {
                    _this.cow.cowHead.rotation -= 0.1;
                    _this.cow.cowHead.body.velocity.x = 0;
                }, null, this);
                this.game.physics.arcade.collide(this.cow.cowHead, this.currentLevel.ground.secondImage, function () {
                    _this.cow.cowHead.rotation -= 0.1;
                    _this.cow.cowHead.body.velocity.x = 0;
                }, null, this);
                if (this.cow.cowHead) {
                    this.cow.cowHead.rotation += 0.1;
                }
            }
            if (this.killerCollection.gameOver && this.cow.currentState !== 10 /* DEAD */) {
                this.gameOver();
            }
        };
        Game.prototype.gameOver = function () {
            var _this = this;
            var maximumScore = localStorage.getItem("maxScore");
            if (maximumScore === null) {
                localStorage.setItem("maxScore", this.killerCollection.score.toString());
            }
            else {
                if (parseInt(maximumScore) <= this.killerCollection.score) {
                    maximumScore = this.killerCollection.score.toString();
                    localStorage.setItem("maxScore", maximumScore);
                }
            }
            console.log("gameOver");
            this.cow.currentState = 10 /* DEAD */;
            var randomGameOverIndex = Math.floor(RNG(0, 2)) + 1;
            console.log(randomGameOverIndex);
            this.game.world.bringToTop(this.killerCollection.killerGroup);
            this.game.world.bringToTop(this.topGroup);
            this.killerCollection.bloodEmitter.start(this.cow.states.position.x, this.cow.states.position.y - 200, true, 2000, 30);
            this.game.time.events.add(3000, function () {
                _this.game.state.start("GameOver", false, false, randomGameOverIndex, _this.killerCollection.score, maximumScore);
            }, this);
        };
        Game.prototype.cowThrowItems = function () {
            if (this.grenadeButton.shouldThrowItem) {
                this.grenadeButton.shouldThrowItem = false;
                this.thrownItems.push(this.cow.spawnGrenade());
            }
            if (this.knifeButton.shouldThrowItem) {
                this.knifeButton.shouldThrowItem = false;
                this.thrownItems.push(this.cow.spawnKnife());
            }
            if (this.shotgunButton.shouldThrowItem) {
                this.shotgunButton.shouldThrowItem = false;
                this.cow.shootShotgun(this.butcherList, this.killerCollection, this.pickupItems);
            }
            if (this.gmoButton.shouldThrowItem) {
                this.gmoButton.shouldThrowItem = false;
                this.cow.pickUpGMO();
            }
        };
        Game.prototype.thrownItemsUpdate = function () {
            var _this = this;
            this.thrownItems.forEach(function (item) {
                item.rotation += 0.2;
                if (item.x > _this.game.width) {
                    _this.thrownItems.splice(_this.thrownItems.indexOf(item), 1);
                    item.destroy();
                }
            });
        };
        Game.prototype.pickupItemsUpdate = function () {
            var _this = this;
            this.pickupItems.forEach(function (item) {
                item.rotation -= 0.2;
                _this.game.physics.arcade.collide(item, _this.currentLevel.ground.image, function () {
                    item.rotation += 0.2;
                    item.body.velocity.x = -500;
                });
                _this.game.physics.arcade.collide(item, _this.currentLevel.ground.secondImage, function () {
                    item.rotation += 0.2;
                    item.body.velocity.x = -500;
                });
                _this.game.physics.arcade.overlap(item, _this.cow.states, function () {
                    _this.cow.pickupKnife();
                    _this.pickupItems.splice(_this.pickupItems.indexOf(item), 1);
                    _this.knifeButton.pickUpItem(1);
                    item.body.setSize(0, 0);
                    item.destroy();
                }, null, _this.cow);
            });
        };
        Game.prototype.spawnChests = function () {
            if (this.randomChestsSpawnTime < this.game.time.now) {
                this.randomChestsSpawnTime = this.game.time.now + Math.abs(RNG(this.currentDifficulty.rngChestLow, this.currentDifficulty.rngChestHigh) * 1000);
                var chest = new CowVsButcher.Chest(this.game);
                this.topGroup.add(chest.chests);
                this.chestList.push(chest);
            }
        };
        Game.prototype.spawnButchers = function () {
            if (this.randomButcherSpawnTime < this.game.time.now) {
                this.randomButcherSpawnTime = this.game.time.now + Math.abs(RNG(this.currentDifficulty.rngButchLow, this.currentDifficulty.rngButchHigh) * 1000);
                var sampleButch = new CowVsButcher.Butcher(this.game, this.currentDifficulty.butchSpeed);
                this.killerCollection.killerGroup.addChildAt(sampleButch.states, 6);
                this.killerCollection.killerGroup.addChildAt(sampleButch.states2, 6);
                this.butcherList.push(sampleButch);
            }
        };
        Game.prototype.updateDifficulties = function () {
            if (this.killerCollection.score === 10 && this.shouldChangeDifficulty) {
                this.triggerLevel(1);
            }
            if (this.killerCollection.score === 20 && this.shouldChangeDifficulty) {
                this.triggerLevel(2);
            }
            if (this.killerCollection.score === 11 || this.killerCollection.score === 21) {
                this.shouldChangeDifficulty = true;
            }
        };
        Game.prototype.render = function () {
            //this.game.debug.text(this.game.time.fps.toString() || '--', 10, 70, "#ff0000", "40px Arial");
            //this.game.debug.body(this.cow.states, "#ff0000", false);
            //this.game.debug.body(this.cow.states2, "#00FF00", false);
            //if (this.butcherList[0]) {
            //    this.game.debug.body(this.butcherList[0].states, "#F00", false);
            //}
            //if (this.butcherList[1]) {
            //    this.game.debug.body(this.butcherList[1].states, "#F00", false);
            //}
            //if (this.thrownItems[0]) {
            //    this.game.debug.body(this.thrownItems[0], "#0F0", false);
            //}
            //this.game.debug.text(this.game.time.now.toString() || '--', 10, 100, "#ff0000", "40px Arial");
            //this.game.debug.text(this.butcherList.length.toString() + " " + this.thrownItems.length.toString(), 10, 130, "#ff0000", "40px Arial");
            //
            //if(this.chest){
            //    this.game.debug.body(this.chest.chests,"#ff0000",false);
            //}
            //this.currentLevel.platforms.forEach((platform)=>{
            //    this.game.debug.body(platform,"#ff0000",false);
            //});
            var _this = this;
            this.currentLevel.barbeques.forEach(function (bbq) {
                _this.game.debug.body(bbq.barbeque, "#ff0000", false);
            });
        };
        Game.prototype.triggerLevel = function (difficultyIndex) {
            var _this = this;
            this.shouldChangeDifficulty = false;
            this.currentDifficulty = this.difficulties[difficultyIndex];
            this.butcherList.forEach(function (butch) {
                _this.killerCollection.killerGroup.remove(butch);
                _this.butcherList.splice(_this.butcherList.indexOf(butch), 1);
                butch.states.destroy();
            });
            var gameTween = this.game.add.tween(this.game.world);
            gameTween.to({ alpha: 0 }, 200, Phaser.Easing.Default, true, 0, 0, true);
            gameTween.onLoop.add(function () {
                if (_this.currentLevel === _this.levelOne) {
                    _this.currentLevel = _this.levelTwo;
                    _this.game.world.bringToTop(_this.levelTwo.levelGroup);
                    _this.game.world.bringToTop(_this.killerCollection.killerGroup);
                    _this.game.world.bringToTop(_this.topGroup);
                    _this.game.world.alpha = 0;
                }
                else {
                    _this.currentLevel = _this.levelOne;
                    _this.game.world.bringToTop(_this.levelOne.levelGroup);
                    _this.game.world.bringToTop(_this.killerCollection.killerGroup);
                    _this.game.world.bringToTop(_this.topGroup);
                }
            }, this);
        };
        return Game;
    })(Phaser.State);
    CowVsButcher.Game = Game;
})(CowVsButcher || (CowVsButcher = {}));
//# sourceMappingURL=Game.js.map
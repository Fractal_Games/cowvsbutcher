/// <reference path="../Lib/phaser.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CowVsButcher;
(function (CowVsButcher) {
    var GameOver = (function (_super) {
        __extends(GameOver, _super);
        function GameOver() {
            _super.apply(this, arguments);
        }
        GameOver.prototype.init = function (gameOverScreenIndex, score, maxScore) {
            this.gameOverScreenIndex = gameOverScreenIndex;
            this.score = score;
            this.maxScore = maxScore;
        };
        GameOver.prototype.create = function () {
            this.game.add.image(0, 0, "gameOver" + this.gameOverScreenIndex);
            var playButton = this.game.add.button(this.game.width * 0.76, this.game.height * 0.01, "PlayButton", this.startGame, this);
            var quitButton = this.game.add.button(this.game.width * 0.76, this.game.height * 0.15, "QuitButton", this.exitGame, this);
            playButton.scale.set(0.7);
            quitButton.scale.set(0.7);
            var killsStyle = { fill: "#4e0a09" };
            var killsText = this.game.add.text(25, this.game.height * 0.7, "KILLS: " + this.score.toString(), killsStyle);
            killsText.font = "28-days-later";
            killsText.fontSize = 90;
            killsText.fontWeight = "normal";
            var bestStyle = { fill: "#4e0a09" };
            var bestText = this.game.add.text(25, this.game.height * 0.85, "BEST: " + this.maxScore, bestStyle);
            bestText.font = "28-days-later";
            bestText.fontSize = 90;
            bestText.fontWeight = "normal";
        };
        GameOver.prototype.startGame = function () {
            this.game.state.start("Game");
        };
        GameOver.prototype.exitGame = function () {
            this.game.destroy();
        };
        return GameOver;
    })(Phaser.State);
    CowVsButcher.GameOver = GameOver;
})(CowVsButcher || (CowVsButcher = {}));
//# sourceMappingURL=GameOver.js.map
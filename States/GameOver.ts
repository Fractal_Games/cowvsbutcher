/// <reference path="../Lib/phaser.d.ts"/>

module CowVsButcher{
    export class GameOver extends Phaser.State{
        gameOverScreenIndex:number;
        score:number;
        maxScore:string;

        init(gameOverScreenIndex:number, score:number,maxScore:string){
            this.gameOverScreenIndex=gameOverScreenIndex;
            this.score=score;
            this.maxScore = maxScore;
        }

        create(){

            this.game.add.image(0,0,"gameOver"+this.gameOverScreenIndex);

            var playButton = this.game.add.button(this.game.width * 0.76, this.game.height*0.01, "PlayButton", this.startGame, this);
            var quitButton = this.game.add.button(this.game.width * 0.76, this.game.height * 0.15, "QuitButton", this.exitGame, this);

            playButton.scale.set(0.7);
            quitButton.scale.set(0.7);

            var killsStyle = {fill: "#4e0a09"};
            var killsText = this.game.add.text(25, this.game.height * 0.7, "KILLS: " + this.score.toString(), killsStyle);
            killsText.font = "28-days-later";
            killsText.fontSize = 90;
            killsText.fontWeight = "normal";

            var bestStyle = {fill: "#4e0a09"};
            var bestText = this.game.add.text(25, this.game.height * 0.85, "BEST: " + this.maxScore, bestStyle);
            bestText.font = "28-days-later";
            bestText.fontSize = 90;
            bestText.fontWeight = "normal";
        }

        startGame() {
            this.game.state.start("Game");
        }

        exitGame() {
            this.game.destroy();
        }
    }
}
/// <reference path="../Lib/phaser.d.ts"/>

    module CowVsButcher{
        export class Blood extends Phaser.Particle{

        game: Phaser.Game;
        radius:number;
        x:number;
        y:number;

        constructor(game:Phaser.Game, x:number, y:number,key?:any){
            this.game = game;
            this.radius = 5 + Math.round(RNG(0,5));
            super(this.game,x,y, game.cache.getBitmapData("Blood"));
            this.x=x;
            this.y=y;
            this.createBlood();
        }

        update(){
            if(this.alpha>0.01)
            {
                this.alpha=this.alpha-(0.005 + Math.random() * (0.009 - 0.0005));
            }
            super.update();
        }

        createBlood(){
            var bmd = this.game.add.bitmapData(this.radius*2,this.radius*2,"Blood");
            bmd.circle(bmd.width/2,bmd.height/2,this.radius,"#8B1914");

            this.game.cache.addBitmapData("Blood",bmd);
        }
    }
}
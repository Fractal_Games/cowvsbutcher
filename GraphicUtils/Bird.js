/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="..\Levels\KillerCollection.ts"/>
var CowVsButcher;
(function (CowVsButcher) {
    var Bird = (function () {
        function Bird(game, key, velocity, y, group) {
            this.game = game;
            this.sprite = this.game.add.sprite(this.game.width, y, "Bird");
            this.sprite.animations.add("Fly", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 30);
            this.sprite.animations.play("Fly", null, true);
            this.sprite.anchor.set(0.5, 0.5);
            this.sprite.scale.set(-1, 1);
            this.velocity = velocity;
            this.period = 0;
            this.bloodThrown = false;
            this.amplitude = RNG(0, 7);
            this.periodIncrement = RNG(0.15, 0.2);
        }
        Bird.prototype.update = function (killerCollection) {
            this.sprite.position.x -= this.velocity;
            this.sprite.position.y = this.amplitude * Math.sin(this.period) + this.sprite.position.y;
            this.period += this.periodIncrement;
            if (this.sprite.position.x <= 100 && !this.bloodThrown) {
                this.bloodThrown = true;
                killerCollection.bloodEmitter.start(50, 80, true, 800, 50);
                killerCollection.featherEmitter.start(50, 80, true, 1500, 5);
                killerCollection.firstSaw.triggerSaw();
            }
        };
        return Bird;
    })();
    CowVsButcher.Bird = Bird;
})(CowVsButcher || (CowVsButcher = {}));
//# sourceMappingURL=Bird.js.map